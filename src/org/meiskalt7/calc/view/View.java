package org.meiskalt7.calc.view;

import org.meiskalt7.calc.model.Calc;

import javax.swing.*;
import java.awt.*;

public class View extends JFrame {

    private JButton bCalc = new JButton("Calculate!");
    private JTextArea textArea = new JTextArea("5+10*25 - 70 / 35");
    private JLabel lResult = new JLabel("Result here");
    private Calc calc;

    public View() {
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(300, 300, 400, 200);
        setLocationRelativeTo(null);
        setLayout(new GridLayout(3, 1));
        setResizable(false);

        bCalc.addActionListener(e -> calculate());
        textArea.setLineWrap(true);
        lResult.setVerticalTextPosition(JLabel.CENTER);
        lResult.setHorizontalAlignment(JLabel.CENTER);

        add(textArea);
        add(bCalc);
        add(lResult);
    }

    public View(Calc calc) {
        this();
        this.calc = calc;
    }


    public void calculate() {
        if (isValid(textArea.getText())) {
            calc.setExpression(textArea.getText());
            try {
                lResult.setText("" + calc.getResult());
            } catch (NumberFormatException e) {
                showError();
            }
        }
    }

    private boolean isValid(String expression) {
        if (expression.contains("(") || expression.contains(")") || expression.contains(",")) {
            showError();
            return false;
        } else
            return true;
    }

    private void showError() {
        lResult.setText("Error: Wrong expression, check and correct it");
    }
}
