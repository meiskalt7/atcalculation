package org.meiskalt7.calc;

import org.meiskalt7.calc.model.Calc;
import org.meiskalt7.calc.view.View;

public class Main {
    public static void main(String[] args) {
        Calc calc = new Calc();
        View view = new View(calc);
    }
}
