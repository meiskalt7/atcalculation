package org.meiskalt7.calc.model;

import java.util.LinkedList;

public class Calc {

    /*
        На вход программы поступает строка вида "5+10*25 - 70 / 35"
    задача программы - вычислить значение введенного выражения с учетом приоритета операций

    ограничения
     + строка произвольной длины
     + произвольные рациональные числа, разделитель целой и дробной части - точка
     + дробные числа пишутся всегда в формате <целая часть>.<дробная часть>, т.е. допустимо написать "45.3323" или "0.134", но не допустимо ".234"
     + возможна запись числа с лидирующими нулями или замыкающими нулями в дробной части, т.е. числа "024" и "24.5000" допустимы
     + унарный минус не учитывать, т.е. запись типа "5 + -10" недопустима, так же как недопустима запись "-5 + 10"
     + поддерживаются 4 операции - сложение, вычитание, умножение, деление
     + вычисление происходит с учетом приоритета операций
     + менять приоритет скобками нельзя, т.е. скобки недопустимы
     + между числами и арифметическими знаками допустимо произвольное число пробелов
     + пробелы недопустимы внутри числа, т.е. число "1 000" не допустимо
         */
    private String expression;

    private LinkedList<Character> oper = new LinkedList<>();
    private LinkedList<Double> numb = new LinkedList<>();

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression.trim();
        oper = new LinkedList<>();
        numb = new LinkedList<>();
    }

    public Calc() {
    }

    public double getResult() {
        //в польскую нотацию
        //5+10*25 - 70 / 35 to + 5 * 10 25 - / 70 35
        parseExpression();
        printLists();
        //Рассчет(первый проход - умножение/деление, второй (+-))
        calculate();
        printLists();
        return numb.getFirst();
    }

    private void parseExpression() throws NumberFormatException {
        int i = 0;
        StringBuilder temp = new StringBuilder();
        while (i < expression.length()) {
            if (isOperation(i)) {

                if (oper.size() == 0) oper.add(expression.charAt(i));

                else if (getPriority(oper.getLast()) >= (getPriority(expression.charAt(i)))) {

                    oper.addLast(expression.charAt(i));

                } else if (getPriority(oper.getLast()) < (getPriority(expression.charAt(i)))) {

                    oper.addLast(expression.charAt(i));

                }

                if (temp.toString().trim().contains("" + ' ')) throw new NumberFormatException();

                numb.addLast(Double.parseDouble(temp.toString()));
                temp.delete(0, temp.length());

            } else if (isNumber(i)) temp.append(expression.charAt(i));

            if (temp.toString().indexOf(".") == 0) {
                throw new NumberFormatException();
            }
            i++;
        }
        numb.addLast(Double.parseDouble(temp.toString()));
    }

    private void calculate() {
        int i = 0;
        while (i < numb.size() - 1) {
            if (getPriority(oper.get(i)) == 1) {
                double a = numb.get(i), b = numb.get(i + 1);
                switch (oper.get(i)) {
                    case '*':
                        numb.set(i, a * b);
                        break;
                    case '/':
                        numb.set(i, a / b);
                        break;
                }
                numb.remove(i + 1);
                oper.remove(i);
            } else i++;
        }

        i = 0;
        while (i < numb.size() - 1) {
            double a = numb.get(i), b = numb.get(i + 1);
            switch (oper.get(i)) {
                case '+':
                    numb.set(i, a + b);
                    break;
                case '-':
                    numb.set(i, a - b);
                    break;
            }
            numb.remove(i + 1);
            oper.remove(i);
        }
    }

    private int getPriority(char oper) {
        switch (oper) {
            case '+':
            case '-':
                return 0;
            case '*':
            case '/':
                return 1;
            default:
                return 0;
        }
    }

    private boolean isOperation(int i) {
        return expression.charAt(i) == '+' || expression.charAt(i) == '-' || expression.charAt(i) == '*' || expression.charAt(i) == '/';
    }

    private boolean isNumber(int i) {
        return expression.charAt(i) >= '0' && expression.charAt(i) <= '9' || expression.charAt(i) == '.' || expression.charAt(i) == ' ';
    }

    private void printLists() {
        System.out.println(oper);
        System.out.println(numb);
    }
}
